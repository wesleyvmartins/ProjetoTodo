<?php

namespace App\Controller;

use App\Entity\Tarefa;
use App\Form\TarefaType;
use App\Repository\TarefaRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TarefasController extends AbstractController
{
    /**
     * @Route("/tarefa", name="tarefas")
     */
    public function index(TarefaRepository $tarefaRepo): Response
    {
        $tarefa = $tarefaRepo->findAll();

        return $this->render('tarefas/index.html.twig', [
            'tarefas' => $tarefa
        ]);
    }

    /**
     * @Route("/tarefa/cria", name="cria_tarefa")
     */
    public function cria(Request $request): Response
    {
       $tarefa = new Tarefa();

       $formulario = $this->createForm(TarefaType::class, $tarefa);
       $formulario->handleRequest($request);

       if ($formulario->isSubmitted()) {
           $entityManager = $this->getDoctrine()->getManager();
           $entityManager->persist($tarefa);
           $entityManager->flush();

           return $this->redirect($this->generateUrl('tarefas'));
       }

        return $this->render('tarefas/cria.html.twig', [
            'formulario' => $formulario->createView()
        ]);
    }

    /**
     * @Route("/tarefa/mostra/{id}", name="mostra_tarefa")
     */
    public function mostra(Tarefa $tarefa):Response
    {
        return $this->render('tarefas/mostra.html.twig', [
            'tarefa' => $tarefa
        ]);
    }

    /**
     * @Route("/tarefa/remove/{id}", name="remove_tarefa" )
     */
    public function remove(Tarefa $tarefa): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($tarefa);
        $entityManager->flush();

        return $this->redirect($this->generateUrl('tarefas'));
    }

    /**
     * @Route("/tarefa/edita/{id}", name="edita_tarefa")
     */
    public function edita(Request $request, Tarefa $tarefa): Response
    {
        $formulario = $this->createForm(TarefaType::class, $tarefa);
        $formulario->handleRequest($request);

        if ($formulario->isSubmitted()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirect($this->generateUrl('tarefas'));
        }

        return $this->render('tarefas/edita.html.twig', [
           'formulario' => $formulario->createView()
        ]);
    }
}
